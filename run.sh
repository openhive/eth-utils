#!/bin/bash
geth --identity "MyNodeName" \
     --rpc --rpccorsdomain "*" \
     --rpcaddr "192.168.10.58" \
     --datadir "~/eth/private" \
     --nodiscover \
     --rpcapi "db,eth,net,web3" \
     --autodag \
     --networkid 1900 \
     --mine \
     --nat "any" \
     console
